# name: Support link Lmss
# about: Formats Overwatch hero names in posts, prepending hero icons and correcting hero spelling and character case.
# version: 1.0
# authors: Daniel Marquard

#register_asset "stylesheets/overwatch-hero-icons.css"
